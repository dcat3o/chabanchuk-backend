const puppeteer = require('puppeteer');
const { EventEmitter } = require('events');
const { Logger } = require('../index');

const BONCH_URL = 'https://lk.sut.ru/';
const BROWSER_LAUNCH_OPTIONS = {
  headless: false
};
const MARKER_DEFAULT_OPTIONS = {
  closeAfter: true
};

class Marker extends EventEmitter {
  static #instance;
  #options;

  constructor() {
    super();
    this.#options = MARKER_DEFAULT_OPTIONS;
  }

  static getInstance() {
    if (!this.#instance) {
      this.#instance = new Marker();
    }
    return this.#instance;
  }

  setOptions(options) {
    this.#options = options;
  }

  #getBonchPage = async () => {
    const browser = await puppeteer.launch(BROWSER_LAUNCH_OPTIONS);
    const page = await browser.newPage();
    page.on('dialog', (dialog) => {
      dialog.accept();
    });
    await page.goto(BONCH_URL, { waitUntil: 'networkidle2' });
    return page;
  }

  #signIn = async (page, email, password) => {
    const emailFieldSelector = '#users';
    const passwordFieldSelector = '#parole';
    const signInButtonSelector = '#logButton';
    const titleItemSelector = '.title_item';
    await page.type(emailFieldSelector, email);
    await page.type(passwordFieldSelector, password);
    await page.click(signInButtonSelector);
    await page.waitForSelector(titleItemSelector, { visible: true, timeout: 3000 });
  }

  #goToTimetable = async (page) => {
    const titleItemSelector = '.title_item';
    const timetableLinkSelector = 'a[title="Расписание"]';
    await page.click(titleItemSelector);
    await page.waitForTimeout(1500);
    await page.click(timetableLinkSelector);
    await page.waitForTimeout(1000);
  }

  #getLessonData = async (page) => {
    const todayLessonRowSelector = 'tr[style*="background: #FF9933"]';
    const todayLessonRows = await page.$$(todayLessonRowSelector);
    todayLessonRows.forEach(row => {

      // TODO

    	const cells = row.cells;
    	const lessonNameCell = cells[1];
    	const lessonName = lessonNameCell.children[0].textContent;
    	const lessonType = lessonNameCell.children[1].textContent;
    	const teacherNameCell = cells[3];
    	const teacherName = teacherNameCell.textContent;
    	console.log(`${lessonName} (${lessonType}) <${teacherName}>`);
    });

    return 'test';
  }

  #startLesson = async (page) => {
    const startLessonButtonSelector = 'a[href*="https://"]';// 'a[onclick*="open_zan"]';
    const startLessonButton = await page.$(startLessonButtonSelector);
    if (!startLessonButton) {
      throw new Error('Lesson not found');
    }
    await startLessonButton.click();
  }

  async markUser(user, options=this.#options) {
    let page;
    let lesson;
    try {
      const { _id, email, password } = user;
      page = await this.#getBonchPage();
      Logger.debug('Got page.');
      await this.#signIn(page, email, password);
      Logger.debug('Signed in.');
      await this.#goToTimetable(page);
      Logger.debug('Got to timetable.');
      lesson = await this.#getLessonData(page);
      Logger.debug('Got lesson data.');
      await this.#startLesson(page);
      Logger.debug('Started the lesson.');
      await this.emit('marked', user, lesson);
      Logger.debug('Emitted the marked event.');
    } catch (e) {
      if (e.name === 'TimeoutError' && e.message.includes('waiting for selector')) {
        e = new Error('Invalid credentials');
      }
      await this.emit('error', e, user, lesson);
    } finally {
      if (options.closeAfter === true) {
        await page.browser().close();
        Logger.debug('Closed the browser.');
      }
    }
  }

  async verifyCredentials(email, password, options=this.#options) {
    let page;
    try {
      page = await this.#getBonchPage();
      Logger.debug('Got page.');
      await this.#signIn(page, email, password);
      Logger.debug('Signed in.');
      return true;
    } catch (e) {
      if (e.name === 'TimeoutError' && e.message.includes('waiting for selector')) {
        e = new Error('Invalid credentials');
        return false;
      }
      await this.emit('error', e, user, lesson);
    } finally {
      if (options.closeAfter === true) {
        await page.browser().close();
        Logger.debug('Closed the browser.');
      }
    }
  }
}

module.exports = Marker;
