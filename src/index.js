require('dotenv').config();
const pino = require('pino');
const Logger = pino({
  name: 'Logger',
  prettyPrint: true,
  level: 'debug'
});

const Client = {
  Logger: Logger
};

start();

async function start() {
  await initializeDao();
  Logger.info('Initialized database.');
  await addTestUser();
  Logger.info('Added test user.');
  await initializeMarker();
  Logger.info('Initialized marker.');
  await initializeAnalytics();
  Logger.info('Initialized analytics module.');
  await initializeVKBot();
  Logger.info('Initialized VK bot.');
}

async function addTestUser() {
  const { MarkMode, SubscriptionType } = require('./util');
  const { User } = Client.DAO;
  await User.Model.findByIdAndDelete(221496793);
  // Умоляю, не ломай мне лк бонча, мне просто было впадлу скрывать эти данные для дебаггинга, энивей я их удалю позже ^^
  await User.create(221496793, 'neraskrytiypotantseval@yandex.ru', 'ae98fbhgfG', MarkMode.ALL, SubscriptionType.TRIAL_ONGOING);
}

async function initializeDao() {
  const mongoose = require('mongoose');
  const DAO = require('./dao');

  mongoose.connect(process.env.DB_URI, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });

  mongoose.connection.on('error', function (err) {
    Logger.error(err.message, 'Got error while connecting to database.');
  });

  mongoose.connection.on('open', function () {
    Logger.debug('Connected to database.');
  });

  DAO.connection = mongoose.connection;

  Client.DAO = DAO;
}

async function initializeMarker() {
  const schedule = require('node-schedule');
  const { Dcrypt } = require('./util');
  const Marker = require('./marker').getInstance();
  const { DAO } = Client;

  async function markEveryone() {
    Logger.debug('Started to mark everyone...');

    Marker.on('marked', (user, lesson) => {
      Logger.info(`Marked user with id '${user._id}' with lesson '${lesson}'.`);
    });

    Marker.on('error', (err, user, lesson) => {
      Logger.error(err, `Got error while marking a user ${user} and lesson ${lesson}.`)
    });

    const subscribedUsers = await DAO.User.getSubscribed();

    subscribedUsers.forEach(async user => {
      user.email = await Dcrypt.decrypt(user.email);
      user.password = await Dcrypt.decrypt(user.password);
      await Marker.markUser(user, { closeAfter: true });
    });
  }

  const jobs = [];
  const cronExpressions = [
    '0 9 * * 1-6',       // First lesson
    '45 10 * * 1-6',    // Second lesson
    '0 13 * * 1-6',     // And so on...
    '45 14 * * 1-6',
    '30 16 * * 1-6',
    '15 18 * * 1-6',
    '0 20 * * 1-6',
    '0 55 4 * * *'
  ];

  await cronExpressions.forEach(expression => {
    const job = schedule.scheduleJob(expression, markEveryone);
    jobs.push(job);
  });

  Marker.jobs = jobs;
  Logger.debug('Created jobs for each lesson.');

  // QUESTION: What if the 'start lesson' button didn't show up?

  Client.Marker = Marker;
}

async function initializeAnalytics() {
  Client.Analytics = require('./analytics').getInstance();
}

async function initializeVKBot() {
  const VKBot = {};
  // TODO: (!!!)
  Logger.warn('Implement VK bot!');
  Client.VKBot = VKBot;
}

module.exports = Client;



/*
require('dotenv').config();
const fs = require('fs');
const Path = require('path');
const mongoose = require('mongoose');
const { User } = require('./dao');
const { SubscriptionType, dcrypt, apiOptions, VKKeyboard, getRandomId } = require('./util');
const { VKApi, BotsLongPollUpdatesProvider } = require('node-vk-sdk');

const api = new VKApi(apiOptions);
const updatesProvider = new BotsLongPollUpdatesProvider(api, process.env.GROUP_ID);
const commands = new Map();

mongoose.connect(process.env.DB_URI, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });

mongoose.connection.once('open', async function () {
  console.log('Connected to database.');
  setCommands();
  console.log('Set all commands.');
  updatesProvider.getUpdates(onUpdates);
});

function setCommands() {
  const commandsPath = Path.join(__dirname, 'commands');
  const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
  for (const file of commandFiles) {
    const command = require(Path.join(commandsPath, file));
    commands.set(command.name, command);
    console.log(`Set command ${command.name}.`);
  }
}

function onUpdates(updates) {
  if (updates[0]) {
    const currentUpdate = updates[0];
    console.log(`Got new update. Type: '${currentUpdate.type}'.`);
    if (currentUpdate.type === 'message_new') {
      conversate(currentUpdate.object);
    } else if (currentUpdate.type === 'vkpay_transaction') {
      console.log('payment has come!');
      // check this user's credentials in database:
        // if db has him, change sub type to 'full':
        // if db doesn't have him, go through sub process:
    }
    else {
      console.log('No method for this update type.');
    }
  }
}

async function conversate(update) {
  const message = update.message;
  console.log(`It is a message. Text: ${message.text}`);
  let command;
  if (message.payload) {
    const payload = JSON.parse(message.payload).command;
    command = commands.get(payload);
  }
  if (!command) {
    command = commands.get(message.text);
  }
  if (!command) {
    // send "i dont understand you" message:
    random_id = await api.messagesSend({
      user_id: message.from_id,
      random_id: random_id,
      message: "Didn't understand you!"
    });
  } else {
    try {
      command.execute(message);
    } catch (e) {
      console.log('some error while executing command!');
      return console.error(e);
    }
  }
}*/

/* Пример использования VKKeyboard:
if (message.payload) {
  const command = JSON.parse(message.payload).command;
  console.log(`It was a command: ${command}.`);
}
const keyboard = new VKKeyboard()
  .setOneTime(true)
  .setInline(false);
const unsubscribeButton = new VKKeyboard.TextButton()
  .setLabel('Не отмечать')
  .setColor('negative')
  .setPayload({command: 'unsubscribe'});
const subscribeButton = new VKKeyboard.TextButton()
  .setLabel('Отмечать')
  .setColor('positive')
  .setPayload({command: 'subscribe'});
const paymentHash = `action=pay-to-group&amount=70&description=${encodeURIComponent('На чашку кофе :)')}&group_id=${process.env.GROUP_ID}`;
const payButton = new VKKeyboard.PayButton()
  .setPayload({command: 'pay'})
  .setHash(paymentHash);
keyboard.addButton(unsubscribeButton, 1);
keyboard.addButton(subscribeButton, 1);
keyboard.addButton(payButton, 2);
random_id = await api.messagesSend({
  user_id: message.from_id,
  random_id: getRandomId(),
  message: `You wrote: ${message.text}`,
  keyboard: keyboard.stringify()
});
console.log('Send the reply.');
*/

/*
Перед каждой парой заходить в кабинет с каждого привязанного аккаунта.
Если кнопка присутствует, нажать её.
Если кнопки нет?
  Если пара должна быть в это время, оставить этот аккаунт в списке и через какое-то время повторить процедуру.
  Если пара не должна быть в это время, пропустить аккаунт.

Расписание звонков:
9:00
10:45
13:00
14:45
16:30
18:15
20:00
*/
