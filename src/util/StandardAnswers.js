// const VKKeyboard = require('./VKKeyboard');
// const getRandomId = require('./index');
//
// const answerTemplate = (user_id, message, keyboard) => {
//   return {
//     user_id: user_id,
//     random_id: getRandomId(),
//     message: message,
//     keyboard: keyboard
//   }
// };
// const keyboard = new VKKeyboard()
//   .setOneTime(true)
//   .setInline(false);
//
// const StandardKeyboards = {
//   StartSubscribed: keyboard.addButton(
//     new VKKeyboard.TextButton().setColor('primary').setLabel('Настройки'),
//     1
//   )
//   .addButton(
//     new VKKeyboard.TextButton().setColor('secondary').setLabel('Помощь'),
//     1
//   )
//   .addButton(
//     new VKKeyboard.TextButton().setColor('negative').setLabel('Отписаться'),
//     2
//   ),
//   StartUnsubscribed: keyboard.addButton(
//
//   )
// }
//
// const StandardTexts = {
//   Start: `
//     Привет!
//     Этот бот создан для того, чтобы немного облегчить учёбу в бонче: он будет автоматически отмечать тебя на всех парах в личном кабинете.
//     ❓ Как он работает?
//     Этот бот пользуется различными библиотеками, которые имитируют пользователя и управляют браузером. Он в точности повторяет твои действия: заходит на сайт ЛК, вводит твои данные, переходит к расписанию и нажимает ту самую кнопку "Начать занятие".
//     ❓ Мне нужно вводить свои данные от личного кабинета?
//     Да, придётся ввести свои логин и пароль от личного кабинета. Не пугайся, твои данные тщательно шифруются алгоритмом AES256 и никуда, конечно, не публикуются. Если всё ещё не веришь в безопасность этой темы, то можешь посмотреть исходный код этого проекта здесь: https://vk.cc/aBXKJA.
//     `,
//   test: 'test'
// }
//
//
//
//
// const StartSubscribed = () => {
//   const keyboard = keyboard;
// }
//
// module.exports = {
//   StartSubscribed: StartSubscribed,
//   StartUnsubscribed: StartUnsubscribed
// };
