const crypto = require('crypto');
const algorithm = 'aes-256-ctr';
const secretKey = process.env.SECRET;

class Dcrypt {
  static genIv() {
    return crypto.randomBytes(16);
  }

  static encrypt(text, iv) {
      if (!iv) iv = Dcrypt.genIv();
      if (typeof iv === 'string') iv = Buffer.from(iv, 'hex');
      const cipher = crypto.createCipheriv(algorithm, secretKey, iv);
      const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
      return `${iv.toString('hex')}:${encrypted.toString('hex')}`;

  }

  static decrypt(hash) {
      const [hashIv, hashContent] = hash.split(':');
      const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(hashIv, 'hex'));
      const decrpyted = Buffer.concat([decipher.update(Buffer.from(hashContent, 'hex')), decipher.final()]);
      return decrpyted.toString();
  }

  static compare(text, hash) {
    const [hashIv, hashContent] = hash.split(':');
    return Dcrypt.encrypt(text, hashIv) === hash;
  }
}

module.exports = Dcrypt;
