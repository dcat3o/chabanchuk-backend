const { ConsoleLogger } = require('node-vk-sdk');

exports.apiOptions = {
  lang: 'ru',
  test_mode: 1,
  logger: new ConsoleLogger(),
  token: process.env.ACCESS_TOKEN
};

exports.Dcrypt = require('./Dcrypt');

exports.MarkMode = {
  ALL: 'All',
  DISTANT_ONLY: 'Distant only',     // TODO: Might rename this shit.
  COLLEGE_ONLY: 'College only',       // TODO: And this too.
  OFF: 'Off'
};

exports.SubscriptionType = {
  PAID: 'Paid',
  TRIAL_ONGOING: 'Trial ongoing',
  TRIAL_EXPIRED: 'Trial expired'
}

exports.VKKeyboard = require('./keyboard');

exports.StandardAnswers = require('./StandardAnswers');

exports.getRandomId = () => Math.floor(Math.random()*1000000);
