const Button = require('./Button');

class VKKeyboardOpenLinkButton extends Button {
  constructor() {
    super();
    this._action.type = 'open_link';
  }

  setLabel(label) {
    if (typeof label !== 'string') throw new TypeError('String expected');
    this._action.label = label;
    return this;
  }

  setLink(link) {
    if (typeof link !== 'string') throw new TypeError('String expected');
    this._action.link = link;
    return this;
  }
}

module.exports = VKKeyboardOpenLinkButton;
