const Button = require('./Button');

class VKKeyboardCallbackButton extends Button {
  constructor() {
    super();
    this._action.type = 'callback';
  }

  setLabel(label) {
    if (typeof label !== 'string') throw new TypeError('String expected');
    this._action.label = label;
    return this;
  }
}

module.exports = VKKeyboardCallbackButton;
