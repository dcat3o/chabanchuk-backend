const Button = require('./Button');
const AppsButton = require('./AppsButton');
const CallbackButton = require('./CallbackButton');
const LocationButton = require('./LocationButton');
const OpenLinkButton = require('./OpenLinkButton');
const PayButton = require('./PayButton');
const TextButton = require('./TextButton');

// helper class just to get rid of huge keyboard objects.
// QUESTION: but do i really need this? idk :/

class VKKeyboard {
  static Button = Button;
  static AppsButton = AppsButton;
  static CallbackButton = CallbackButton;
  static LocationButton = LocationButton;
  static OpenLinkButton = OpenLinkButton;
  static PayButton = PayButton;
  static TextButton = TextButton;

  constructor() {
    this._keyboard = {};
    this._keyboard.buttons = [[]];
  }

  toObject() {
    return this._keyboard;
  }

  stringify() {
    return JSON.stringify(this._keyboard);
  }

  setOneTime(bool) {
    if (typeof bool !== 'boolean') throw new TypeError('Boolean expected');
    this._keyboard.one_time = bool;
    return this;
  }

  setInline(bool) {
    if (typeof bool !== 'boolean') throw new TypeError('Boolean expected');
    this._keyboard.inline = bool;
    return this;
  }

  addButton(button, rowNumber) {
    if (!button instanceof Button) throw new TypeError('Button expected');
    if (!rowNumber) rowNumber = this._keyboard.buttons.length;
    if (!this._keyboard.buttons[rowNumber-1]) {
      this._keyboard.buttons[rowNumber-1] = [];
    }
    this._keyboard.buttons[rowNumber-1].push(button.toObject());
    return this;
  }
}

module.exports = VKKeyboard;
