const Button = require('./Button');

class VKKeyboardPayButton extends Button {
  constructor() {
    super();
    this._action.type = 'vkpay';
  }

  setAppId(id) {
    if (typeof id !== 'number') throw new TypeError('Number expected');
    this._action.id = id;
    return this;
  }

  setOwnerId(id) {
    if (typeof id !== 'number') throw new TypeError('Number expected');
    this._action.id = id;
    return this;
  }

  setLabel(label) {
    if (typeof label !== 'string') throw new TypeError('String expected');
    this._action.label = label;
    return this;
  }

  setHash(hash) {
    if (typeof hash !== 'string') throw new TypeError('String expected');
    this._action.hash = hash;
    return this;
  }
}

module.exports = VKKeyboardPayButton;
