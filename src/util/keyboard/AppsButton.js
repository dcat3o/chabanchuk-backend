const Button = require('./Button');

class VKKeyboardAppsButton extends Button {
  constructor() {
    super();
    this._action.type = 'open_app';
  }

  setLabel(label) {
    if (typeof label !== 'string') throw new TypeError('String expected');
    this._action.label = label;
    return this;
  }
}

module.exports = VKKeyboardAppsButton;
