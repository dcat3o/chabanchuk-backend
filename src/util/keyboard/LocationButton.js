const Button = require('./Button');

class VKKeyboardLocationButton extends Button {
  constructor() {
    super();
    this._action.type = 'location';
  }
}

module.exports = VKKeyboardLocationButton;
