const Button = require('./Button');

class VKKeyboardTextButton extends Button {
  constructor() {
    super();
    this._action.type = 'text';
  }

  setLabel(label) {
    if (typeof label !== 'string') throw new TypeError('String expected');
    this._action.label = label;
    return this;
  }
}

module.exports = VKKeyboardTextButton;
