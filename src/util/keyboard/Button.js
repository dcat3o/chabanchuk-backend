class Button {
  constructor() {
    this._button = {};
    this._action = {};
  }

  toObject() {
    this._button.action = this._action;
    return this._button;
  }

  setColor(color) {
    if (!['primary', 'secondary', 'negative', 'positive'].includes(color)) throw new TypeError('Color expected');
    this._button.color = color;
    return this;
  }

  setType(type) {
    if (!['text', 'open_link', 'location', 'vkpay', 'open_app', 'callback'].includes(type)) throw new TypeError('Type expected');
    this._action.type = type;
    return this;
  }

  setPayload(payload) {
    if (typeof payload !== 'object') throw new TypeError('Object expected');
    this._action.payload = JSON.stringify(payload);
    return this;
  }
}

module.exports = Button;
