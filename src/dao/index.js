const User = require('./models/User');
const Subscription = require('./models/Subscription');
const Analytics = require('./models/Analytics');

module.exports = {
  User: User,
  Subscription: Subscription,
  Analytics: Analytics
};
