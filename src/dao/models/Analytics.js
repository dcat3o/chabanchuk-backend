const mongoose = require('mongoose');
const { Schema } = mongoose;

class Analytics {
  static Schema = new Schema({
    _id: Number,
    markedLessons: { type: Map, of: Date }
  });

  static Model = mongoose.model('Analytics', Analytics.Schema);
}

module.exports = Analytics;
