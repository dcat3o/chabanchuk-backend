const mongoose = require('mongoose');
const { Schema } = mongoose;

class Subscription {
  static Schema = new Schema({
    type: { type: String, required: true },
    date: { type: String, default: Date.now, immutable: true }
  });

  static Model = mongoose.model('Subscription', Subscription.Schema);
}

module.exports = Subscription;
