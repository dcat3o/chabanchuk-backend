const mongoose = require('mongoose');
const { Schema } = mongoose;
const { Dcrypt, MarkMode, SubscriptionType } = require('../../util');
const Subscription = require('./Subscription');
const Analytics = require('./Analytics');

class User {
  static Schema = new Schema({
    _id: Number,
    email: { type: String, required: true },
    password: { type: String, required: true },
    markMode: { type: String, required: true },
    subscriptions: {
      current: { type: Subscription.Schema },
      history: [{ type: Subscription.Schema, immutable: true }]
    },
    analytics: { type: Analytics.Schema }
  });

  static Model = mongoose.model('User', User.Schema);

  static async exists(id) {
    return await User.Model.exists({ _id: id });
  }

  static async getById(id) {
    return await User.Model
      .findById(id)
      .exec();
  }

  static async getSubscribed() {
    return await User.Model.find({
      "subscriptions.current.type": { $in: [SubscriptionType.PAID, SubscriptionType.TRIAL_ONGOING] }
    });
  }

  static async create(id, email, password, markMode, subscriptionType) {
    if (await User.exists(id)) throw new Error('User already exists');
    return await User.Model.create({
      _id: id,
      email: Dcrypt.encrypt(email),
      password: Dcrypt.encrypt(password),
      markMode: markMode,
      "subscriptions.current": { type: subscriptionType },
      analytics: { _id: id, markedLessons: new Map() }
    });
  }

  static async changeMarkMode(id, mode) {
    const user = await User.Model.findById(id).exec();
    if (!user) throw new Error('User not found');
    user.markMode = mode;
    return user.save();
  }

  static async changeSubscriptionType(id, type) {
    const user = await User.Model.findById(id).exec();
    if (!user) throw new Error('User not found');
    user.subscriptions.history.push(user.subscriptions.current);
    user.subscriptions.current = { type: type };
    return await user.save();
  }

  static async getCurrentSubscription(id) {
    const user = await User.Model.findById(id).exec();
    if (!user) throw new Error('User not found');
    return user.subscriptions.current;
  }

  static async getSubscriptionHistory(id) {
    const user = await User.Model.findById(id).exec();
    if (!user) throw new Error('User not found');
    return user.subscriptions.history;
  }

  static async addMarkedLesson(id, lesson, date = Date.now()) {
    const user = await User.Model.findById(id).exec();
    if (!user) throw new Error('User not found');
    user.analytics.markedLessons.set(lesson, date);
    return await user.save();
  }

  static async getMarkedLessons(id) {
    const user = await User.Model.findById(id).exec();
    if (!user) throw new Error('User not found');
    return user.analytics.markedLessons;
  }
}

module.exports = User;
