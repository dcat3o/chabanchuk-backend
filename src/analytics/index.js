const { Marker, Logger } = require('../index');

class Analytics {
  static #instance;

  constructor() {
    this.#watchMarked();
    this.#watchError();
  }

  static getInstance() {
    if (!this.#instance) {
      this.#instance = new Analytics();
    }
    return this.#instance;
  }

  #watchMarked = () => {
    Marker.on('marked', async (user, lesson) => {
      user.analytics.markedLessons.set(lesson, Date.now());
      await user.save();
      Logger.debug(`Updated analytics to user with id ${user._id}.`);
    });
  }

  #watchError = () => {
    Marker.on('error', (err, user, lesson) => {
      // todo
      Logger.debug(`Analytics: Got an error from user with id ${user._id}.`)
    });
  }
}

module.exports = Analytics;
